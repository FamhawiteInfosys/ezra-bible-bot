package bibletags

import (
	"io/ioutil"
	"testing"
)

func TestDb(t *testing.T) {
	dbFile, err := ioutil.TempFile("", "bibledb*.sqlite")
	if err != nil {
		t.Errorf("Opening temp file for db operations: %v", err)
		return
	}

	t.Logf("Creating DB in file %s", dbFile.Name())

	btdb, err := OpenDb(dbFile.Name())
	if err != nil {
		t.Errorf("Opening Db: %v", err)
		return
	}

	btdb.Close()

	btdb, err = OpenDb(dbFile.Name())
	if err != nil {
		t.Errorf("Re-opening Db: %v", err)
	}

	btdb.Close()

}

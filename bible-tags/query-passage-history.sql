/* 
 * Find last viewtime for passages with a given tag. NB for some reason
 * 'unixepoch()' doesn't seem to be working, so use strftime('%s', ...) instead.
 */
select passage, min(ifnull(now - strftime('%s', ts), unixyr), unixyr)+1 as secondsago
  from (select strftime('%s') as now, 60*60*24*365 as unixyr)
    left join (select passage
          from passagetags
          where tagname=? and chatid in (?, ?))
    left natural join (
        select passage, max(ts) as ts
            from viewlog
            where viewchatid=?
            group by passage);

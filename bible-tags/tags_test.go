package bibletags

import (
	"io/ioutil"
	"testing"
)

func getTestDb(t *testing.T) *BibleTagDb {
	dbFile, err := ioutil.TempFile("", "bibledb*.sqlite")
	if err != nil {
		t.Fatalf("Opening temp file for db operations: %v", err)
	}

	t.Logf("Creating DB in file %s", dbFile.Name())

	btdb, err := OpenDb(dbFile.Name())
	if err != nil {
		t.Fatalf("Opening Db: %v", err)
	}

	return btdb
}

func TestTags(t *testing.T) {
	btdb := getTestDb(t)

	var deletedchats []struct {
		chatid int64
		userid int64
		tags   map[string][]string
	}

	// Create tags
	chattags := []struct {
		chatid int64
		userid int64
		tags   map[string][]string
	}{
		{chatid: CHATID_GLOBAL,
			userid: USERID_NONE,
			tags: map[string][]string{
				"TMSNewLife": {
					"2 Corinthinans 5:17",
					"Galatians 2:20",
					"Romans 12:1",
					"John 14:21"},
				"TMSProclaimChrist": {
					"Romans 3:23",
					"Isaiah 53:6",
					"Romans 6:23",
					"Hebrews 9:27"},
				"TMSRelyOnGod": {
					"1 Corinthians 3:16",
					"1 Corinthians 2:12",
					"Isaiah 41:10",
					"Philippians 4:13"}}},
		{chatid: 148,
			userid: 148,
			tags: map[string][]string{
				"TMSNewLife": {"2 Timothy 3:16",
					"Joshua 1:8",
					"John 15:7",
					"Philippians 4:6-7",
					"Matthew 18:20"},
				"TMSChristsDisciple": {"Matthew 6:33",
					"Luke 9:23",
					"1 John 2:15-16"}}},
		{chatid: -32839,
			userid: USERID_NONE,
			tags: map[string][]string{
				"TMSNewLife": {"Hebrews 10:24-25",
					"Matthew 4:19",
					"Romans 1:16"},
				"TMSProclaimChrist": {"Romans 5:8",
					"1 Peter 3:18",
					"Ephesians 2:8-9",
					"Titus 3:5"},
				"TMSRelyOnGod": {"Lamentations 3:22-23",
					"Isaiah 53:6",
					"Romans 6:23",
					"Hebrews 9:27"},
				"TMSChristlikeness": {"John 13:34-35",
					"1 John 3:18",
					"Philippians 2:3-4"}}},
	}

	for _, chattag := range chattags {
		t.Logf("Reifying chat %d", chattag.chatid)
		if err := btdb.ReifyChat(chattag.chatid, chattag.userid); err != nil {
			t.Errorf("Reifying chatid %d userid %d: %v", chattag.chatid, chattag.userid, err)
			return
		}
		for tagname, passages := range chattag.tags {
			t.Logf("Creating chatid %d tag %s", chattag.chatid, tagname)
			if err := btdb.NewTag(chattag.chatid, tagname); err != nil {
				t.Errorf("Creating new tag: %v", err)
				return
			}
			for _, passage := range passages {
				t.Logf("Tagging passage %s with chatid %d tag %s", passage, chattag.chatid, tagname)
				if err := btdb.TagPassage(chattag.chatid, tagname, passage); err != nil {
					t.Errorf("Tagging passage %s with chatid %d tag %s: %v", passage, chattag.chatid, tagname, err)
					return
				}
			}
		}
	}

	check := func() {
		for _, chattag := range chattags {
			t.Logf("Getting tags for chatid %d", chattag.chatid)
			gottags, err := btdb.ListTags(chattag.chatid)
			if err != nil {
				t.Errorf("Listing tags: %v", err)
				return
			}
			t.Logf("Got tags %v", gottags)
			wantlen := len(chattag.tags)
			if chattag.chatid != CHATID_GLOBAL {
				wantlen += len(chattags[0].tags)
			}
			if len(gottags) != wantlen {
				t.Errorf("Expecting %d tags, got %d!", wantlen, len(gottags))
			}

			var local, global int
			for _, tag := range gottags {
				// Count up the local & global tags to make sure we have the right proportion
				if tag.IsGlobal {
					global++
					if _, prs := chattags[0].tags[tag.Tagname]; !prs {
						t.Errorf("Unexpected global tag %s", tag.Tagname)
					}
				} else {
					local++
					if _, prs := chattag.tags[tag.Tagname]; !prs {
						t.Errorf("Unexpected local tag %s", tag.Tagname)
					}
				}

				passages, err := btdb.ShowTagPassages(chattag.chatid, tag.Tagname)
				if err != nil {
					t.Errorf("Error getting passages for chatid %d tag %s: %v",
						chattag.chatid, tag.Tagname, err)
				}
				t.Logf("Passages for chatid %d tag %s: %v", chattag.chatid, tag.Tagname, passages)
				if len(passages) < 1 {
					t.Errorf("Expected more than 0 passages!")
				}
			}
			if global != len(chattags[0].tags) {
				t.Errorf("Expected %d global tags, got %d", len(chattags[0].tags), global)
			}
			if chattag.chatid != CHATID_GLOBAL && local != len(chattag.tags) {
				t.Errorf("Expected %d local tags, got %d", len(chattag.tags), local)
			}
		}
	}

	check()

	for _, chattag := range chattags {
		if chattag.chatid == CHATID_GLOBAL {
			continue
		}

		// Always view all tags, to make sure we trigger any foreign key issues w/ deletion below
		for tagname := range chattag.tags {
			for i := 0; i < (len(chattag.tags[tagname])+len(chattags[0].tags[tagname]))*4; i++ {
				if passage, err := btdb.GetTag(chattag.chatid, tagname); err != nil {
					t.Errorf("ERROR Getting chatid %d tag %s: %v", chattag.chatid, tagname, err)
				} else {
					t.Logf("chatid %d tag %s: %s", chattag.chatid, tagname, passage)
				}
			}
		}
	}

	check()

	// Delete one tag from each chatid
	for i, chattag := range chattags {
		// Always leave at least one tag in the list
		if len(chattag.tags) < 2 {
			continue
		}

		// Choose an arbitrary tag
		var victim string
		for k := range chattag.tags {
			victim = k
			break
		}

		t.Logf("Deleting chatid %d tag %s", chattag.chatid, victim)
		if err := btdb.DeleteTag(chattag.chatid, victim); err != nil {
			t.Errorf("Deleting tag: %v", err)
		} else {
			delete(chattags[i].tags, victim)
		}
	}

	check()

	// Delete one passage from each tag
	for i, chattag := range chattags {
		for tagname, passages := range chattag.tags {
			victim := len(passages) - 1
			// Always leave at least one passage in the list
			if victim <= 0 {
				continue
			}

			passage := passages[victim]

			t.Logf("Untagging passage %s from chatid %d tag %s",
				passage, chattag.chatid, tagname)
			if err := btdb.UntagPassage(chattag.chatid, tagname, passage); err != nil {
				t.Errorf("Untagging passage: %v", err)
			} else {
				chattags[i].tags[tagname] = append(chattags[i].tags[tagname][:victim], chattags[i].tags[tagname][victim+1:]...)
			}
		}
	}

	check()

	// Delete one chatid
	{
		victim := 1
		if err := btdb.DeleteChat(chattags[victim].chatid); err != nil {
			t.Errorf("Deleting chatid %d: %v", chattags[victim].chatid, err)
		} else {
			deletedchats = append(deletedchats, chattags[victim])
			chattags = append(chattags[:victim], chattags[victim+1:]...)
		}
	}

	check()

	// TODO: Test failure modes:
	// - Can't delete CHATID_GLOBAL
	if err := btdb.DeleteChat(CHATID_GLOBAL); err == nil {
		t.Errorf("ERROR: Successfully deleted global chatid!")
	}

	// NewTag: Fail on non-existent chat ids, deleted chatid,s fail on duplicate
	// tagnames, fail on tagnames that differ only in case
	if err := btdb.NewTag(-1, "Nonexistent"); err == nil {
		t.Errorf("ERROR: Successfully created tag for non-existent chatid!")
	}
	if err := btdb.NewTag(deletedchats[0].chatid, "Deleted"); err == nil {
		t.Errorf("ERROR: Successfully created tag for deleted chatid!")
	}
	{
		target := &chattags[1]
		if err := btdb.NewTag(target.chatid, "Duplicate"); err != nil {
			t.Errorf("ERROR: Creating first of a pair of duplicate tags: %v", err)
		}
		if err := btdb.NewTag(target.chatid, "Duplicate"); err == nil {
			t.Errorf("ERROR: Created second of a pair of duplicate tags!")
		}
		if err := btdb.NewTag(target.chatid, "duplicate"); err == nil {
			t.Errorf("ERROR: Created a tag which differs only in case!")
		}
	}

	// DeleteTag: Fail on non-existent chatids, deleted chatids, non-existent tags
	// Should succeed if we differ only in case.
	if err := btdb.DeleteTag(-1, "NoChatId"); err == nil {
		t.Errorf("ERROR: Successfully deleted tag with non-existent chatid")
	}
	if err := btdb.DeleteTag(deletedchats[0].chatid, "DeletedChatId"); err == nil {
		t.Errorf("ERROR: Successfully deleted tag for deleted chatid")
	}
	if err := btdb.DeleteTag(chattags[1].chatid, "Nonexistent"); err == nil {
		t.Errorf("ERROR: Successfully deleted non-existent tag")
	}
	if err := btdb.DeleteTag(chattags[1].chatid, "duplicate"); err != nil {
		t.Errorf("ERROR: Failed to delete tag with the wrong case: %v", err)
	}
	// Re-create tag for use below
	if err := btdb.NewTag(chattags[1].chatid, "Duplicate"); err != nil {
		t.Errorf("ERROR: Re-creating duplicate tags: %v", err)
		chattags[1].tags["Duplicate"] = []string{}
	}

	// TagPassage: Fail for non-existent chatids, deleted chatids, non-existent
	// tags, double-tagging a passage.  Should fail tagging the same passage
	// with a diffirent tag capitalization; should succeed tagging a different
	// passage with different capitalization.
	if err := btdb.TagPassage(-1, "NoChatId", "2 Thesselations 6:6"); err == nil {
		t.Errorf("ERROR: Tagging passage w/ non-existent chatid returned success!")
	}
	{
		var tagname string
		for k := range deletedchats[0].tags {
			tagname = k
			break
		}
		if err := btdb.TagPassage(deletedchats[0].chatid, tagname, "1 John 1:1"); err == nil {
			t.Errorf("ERROR: Tagging passage w/ deleted chatid and tag succeeded")
		}
	}
	if err := btdb.TagPassage(chattags[1].chatid, "Nonexistent", "1 John 1:1"); err == nil {
		t.Errorf("ERROR: Tagging passage w/ non-existent tag succeeded!")
	}
	if err := btdb.TagPassage(chattags[1].chatid, "Duplicate", "1 John 1:1"); err != nil {
		t.Errorf("ERROR: Tagging a passage the first time failed")
	} else if err := btdb.TagPassage(chattags[1].chatid, "Duplicate", "1 John 1:1"); err == nil {
		t.Errorf("ERROR: Tagging a passage the second time succeeded")
	}
	if err := btdb.TagPassage(chattags[1].chatid, "duplicate", "1 John 1:1"); err == nil {
		t.Errorf("ERROR: Re-tagging a passage with different tag case succeeded")
	}
	if err := btdb.TagPassage(chattags[1].chatid, "Duplicate", "1 john 1:1"); err == nil {
		t.Errorf("ERROR: Re-tagging a passage with different passage case succeeded")
	}
	if err := btdb.TagPassage(chattags[1].chatid, "duplicate", "1 John 1:2"); err != nil {
		t.Errorf("ERROR: Tagging a passage with different case failed: %v", err)
	}

	// ShowTagPassages: Should succeed if either local or global tag exists,
	// even if no passages; should fail if neither tag exists.  Should fail if
	// tag was deleted.  May succeed for non-existent chatids if a global tag
	// exists.
	if err := btdb.NewTag(CHATID_GLOBAL, "GlobalOnly"); err != nil {
		t.Errorf("ERROR: Creating global-only tag for testing: %v", err)
	}
	if results, err := btdb.ShowTagPassages(chattags[1].chatid, "GlobalOnly"); err != nil {
		t.Errorf("ERROR: Getting global-only tag with no passages: %v", err)
	} else if len(results) != 0 {
		t.Errorf("ERROR: Wrong number of results %d for global-only tags", len(results))
	}
	if results, err := btdb.ShowTagPassages(chattags[1].chatid, "globalonly"); err != nil {
		t.Errorf("ERROR: Getting global-only tag with no capitalization: %v", err)
	} else if len(results) != 0 {
		t.Errorf("ERROR: Wrong number of results %d for global-only tags", len(results))
	}
	if err := btdb.TagPassage(CHATID_GLOBAL, "GlobalOnly", "John 3:16"); err != nil {
		t.Errorf("ERROR: Tagging passage with global-only tag: %v", err)
	}
	if results, err := btdb.ShowTagPassages(chattags[1].chatid, "GlobalOnly"); err != nil {
		t.Errorf("ERROR: Getting global-only tag: %v", err)
	} else if len(results) != 1 {
		t.Errorf("ERROR: Wrong number of results %d for global-only tags", len(results))
	}
	if results, err := btdb.ShowTagPassages(-1, "GlobalOnly"); err != nil {
		t.Errorf("ERROR: Getting global-only tag with non-existent chatid: %v", err)
	} else if len(results) != 1 {
		t.Errorf("ERROR: Wrong number of results %d for global-only tags", len(results))
	}

	if err := btdb.NewTag(chattags[1].chatid, "LocalOnly"); err != nil {
		t.Errorf("ERROR: Creating local-only tag for testing: %v", err)
	}
	if results, err := btdb.ShowTagPassages(chattags[1].chatid, "LocalOnly"); err != nil {
		t.Errorf("ERROR: Getting global-only tag with no passages: %v", err)
	} else if len(results) != 0 {
		t.Errorf("ERROR: Wrong number of results %d for local-only tags", len(results))
	}
	if err := btdb.TagPassage(chattags[1].chatid, "LocalOnly", "John 3:16"); err != nil {
		t.Errorf("ERROR: Tagging passage with local-only tag: %v", err)
	}
	if results, err := btdb.ShowTagPassages(chattags[1].chatid, "LocalOnly"); err != nil {
		t.Errorf("ERROR: Getting local-only tag: %v", err)
	} else if len(results) != 1 {
		t.Errorf("ERROR: Wrong number of results %d for local-only tags", len(results))
	}

	if results, err := btdb.ShowTagPassages(chattags[1].chatid, "Nonexistent"); err == nil {
		t.Errorf("ERROR: ShowTagPassages unexpectedly succeeded for non-existent tag: %v", results)
	}

	if err := btdb.DeleteTag(chattags[1].chatid, "LocalOnly"); err != nil {
		t.Errorf("ERROR: Deleting local tag: %v", err)
	}
	if results, err := btdb.ShowTagPassages(chattags[1].chatid, "LocalOnly"); err == nil {
		t.Errorf("ERROR: ShowTagPassages unexpectedly succeeded for deleted tag: %v", results)
	}

	if err := btdb.DeleteTag(CHATID_GLOBAL, "GlobalOnly"); err != nil {
		t.Errorf("ERROR: Deleting global tag: %v", err)
	}
	if results, err := btdb.ShowTagPassages(chattags[1].chatid, "GlobalOnly"); err == nil {
		t.Errorf("ERROR: ShowTagPassages unexpectedly succeeded for deleted tag: %v", results)
	}

	// UntagPassage: Should fail for non-existent chatids, non-existent
	// tagnames, or non-tagged passage.  Should fail for deleted chatids,
	// deleted tagnames, already-untagged passages.
	// GetTag: Should fail for non-existent chatids, non-existent tagnames, or
	// tags without any passages.

	if err := btdb.UntagPassage(-1, "NoChatId", "Matthew 5:16"); err == nil {
		t.Errorf("ERROR: UntagPassage succeeded for non-existent chatid!")
	}
	if passage, err := btdb.GetTag(-1, "NoChatId"); err == nil {
		t.Errorf("ERROR: GetTag returned passage %s for non-existent chatid!", passage)
	}
	if err := btdb.UntagPassage(chattags[1].chatid, "Nonexistent", "Mathew 5:16"); err == nil {
		t.Errorf("ERROR: UntagPassage succeede for non-existent tag!")
	}
	if passage, err := btdb.GetTag(chattags[1].chatid, "Nonexistent"); err == nil {
		t.Errorf("ERROR: GetTag returned passage %s for non-existent tag!", passage)
	}
	if err := btdb.NewTag(chattags[1].chatid, "UntagPassageTestTag"); err != nil {
		t.Errorf("ERROR: Creating testing tag: %v", err)
	}
	if passage, err := btdb.GetTag(chattags[1].chatid, "UntagPassageTestTag"); err == nil {
		t.Errorf("ERROR: GetTag returned passage %s for empty tag!", passage)
	}
	if err := btdb.UntagPassage(chattags[1].chatid, "UntagPassageTestTag", "Matthew 5:16"); err == nil {
		t.Errorf("ERROR: Untagging non-tagged passage succeeded!")
	}
	if err := btdb.TagPassage(chattags[1].chatid, "UntagPassageTestTag", "Matthew 5:16"); err != nil {
		t.Errorf("ERROR: Tagging passage for testing: %v", err)
	}
	if _, err := btdb.GetTag(chattags[1].chatid, "UntagPassageTestTag"); err != nil {
		t.Errorf("ERROR: GetTag for passage that exists: %v", err)
	}
	if _, err := btdb.GetTag(chattags[1].chatid, "untagpassagetesttag"); err != nil {
		t.Errorf("ERROR: GetTag for passage that exists with different case: %v", err)
	}
	if err := btdb.UntagPassage(chattags[1].chatid, "UntagPassageTestTag", "matthew 5:16"); err != nil {
		t.Errorf("ERROR: Untagging passage for testing with different case: %v", err)
	}
	if err := btdb.UntagPassage(chattags[1].chatid, "UntagPassageTestTag", "Matthew 5:16"); err == nil {
		t.Errorf("ERROR: Untagging untagged passage succeeded!")
	}
	if passage, err := btdb.GetTag(chattags[1].chatid, "UntagPassageTestTag"); err == nil {
		t.Errorf("ERROR: GetTag returned passage %s for untagged!", passage)
	}
	// Tag the passage again, then delete the whole tag
	if err := btdb.TagPassage(chattags[1].chatid, "UntagPassageTestTag", "Matthew 5:16"); err != nil {
		t.Errorf("ERROR: Tagging passage for testing: %v", err)
	}
	if err := btdb.DeleteTag(chattags[1].chatid, "UntagPassageTestTag"); err != nil {
		t.Errorf("ERROR: Deleting tag for testing: %v", err)
	}
	if err := btdb.UntagPassage(chattags[1].chatid, "UntagPassageTestTag", "Matthew 5:16"); err == nil {
		t.Errorf("ERROR: Untagging passage with deleted tag  succeeded!")
	}
	if passage, err := btdb.GetTag(chattags[1].chatid, "UntagPassageTestTag"); err == nil {
		t.Errorf("ERROR: GetTag returned passage %s for deleted tag!", passage)
	}

	// ReifyChat should succeed if re-executed.  What should happen if the
	// chat exists but with a different userid?

	// DeleteChat should fail for non-existent chatids and deleted chatids.

	// Consider: non-existent tags, non-existent chatids, non-tagged passages,
	// deleted tags, deleted chatids, un-tagged passages
	// chatids with no tags, tags with no tagged passages
	// Double-creation
	// Different capitalization

	// TODO: Test parallelism
}

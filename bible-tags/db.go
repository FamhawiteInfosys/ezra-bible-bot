// Package bibletags implements "tagged passage" functionality for EzraBibleBot.
// There's no reason you can't use it in other applications, but it's interface
// is targeted at the Telegram use case.
package bibletags

/* Ezra Bible Bot
Copyright (C) 2022  George Dunlap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	_ "embed"
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/martyros/sqlutil/lifecycle"
	"gitlab.com/martyros/sqlutil/types/timedb"
)

type BibleTagDb struct {
	db *sqlx.DB

	defaultTimeout time.Duration

	reminderRecv    chan Reminder
	reminderCommand chan int

	testTimerMachinery bool

	testNow *timedb.Time
}

var defaultBtdb = BibleTagDb{
	defaultTimeout: time.Hour * 24,
}

//go:embed bible-tags.sql
var bibleTagsSchemaSql string

const bibleTagsSchemaVersion = 1

const (
	testTimerMachinery = "TestTimerMachinery"
)

func OpenDb(filename string) (*BibleTagDb, error) {
	btdb := defaultBtdb

	if filename == testTimerMachinery {
		btdb.testTimerMachinery = true
		return &btdb, nil
	}

	log.Printf("Opening database %s", filename)

	db, err := sqlx.Open("sqlite3", "file:"+filename+"?_fk=true&mode=rwc")
	if err != nil {
		return nil, fmt.Errorf("Opening database: %w", err)
	}

	if err = lifecycle.Schema(bibleTagsSchemaVersion, bibleTagsSchemaSql).Open(db.DB); err != nil {
		db.Close()
		return nil, fmt.Errorf("Initializing schema: %w", err)
	}

	btdb.db = db

	return &btdb, nil
}

func (btdb *BibleTagDb) Close() {
	if btdb.db != nil {
		btdb.db.Close()
		btdb.db = nil
	}
}

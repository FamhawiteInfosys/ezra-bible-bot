package bibletags

/* Ezra Bible Bot
Copyright (C) 2022  George Dunlap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/martyros/sqlutil/txutil"
	"gitlab.com/martyros/sqlutil/types/timedb"
)

const (
	CHATID_GLOBAL = 0
	USERID_NONE   = 0
)

// ReifyChat will create a record for a chat if it doesn't exist.
func (btdb *BibleTagDb) ReifyChat(chatid int64, userid int64) error {
	if _, err := btdb.db.Exec(
		"insert or ignore into chats(chatid, userid, timezone) values (?, ?, ?)",
		chatid, userid, timedb.Location{Location: time.UTC}); err != nil {
		return fmt.Errorf("Inserting chatid %d userid %d: %w",
			chatid, userid, err)
	}
	return nil
}

func (btdb *BibleTagDb) DeleteChat(chatid int64) error {
	if chatid == CHATID_GLOBAL {
		return fmt.Errorf("Cannot delete global chatid %d", chatid)
	}

	return txutil.TxLoopDb(btdb.db, func(eq sqlx.Ext) error {
		/* FIXME: Delete reminders */

		if _, err := eq.Exec(
			`update viewlog set tagchatid=NULL, tagname=NULL 
				where tagchatid=?`, chatid); err != nil {
			return fmt.Errorf("Replacing tag references with NULL: %w", err)
		}

		if _, err := eq.Exec(
			`update viewlog set viewchatid=NULL
				where viewchatid=?`, chatid); err != nil {
			return fmt.Errorf("Replacing viewchatid references with NULL: %w", err)
		}

		if _, err := eq.Exec(`delete from passagetags where chatid=?`, chatid); err != nil {
			return fmt.Errorf("Removing passagetags for chatid %d: %w", chatid, err)
		}

		if _, err := eq.Exec(`delete from tags where chatid=?`, chatid); err != nil {
			return fmt.Errorf("Deleting tags for chat %d: %w", chatid, err)
		}

		if _, err := eq.Exec(`delete from chats where chatid=?`, chatid); err != nil {
			return fmt.Errorf("Deleting chat %d: %w", chatid, err)
		}

		return nil
	})
}

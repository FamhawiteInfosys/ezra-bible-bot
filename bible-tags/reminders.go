package bibletags

/* Ezra Bible Bot
Copyright (C) 2022  George Dunlap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/martyros/sqlutil/txutil"
	"gitlab.com/martyros/sqlutil/types/timedb"
)

func (btdb *BibleTagDb) now() timedb.Time {
	if btdb.testNow != nil {
		return *btdb.testNow
	}
	return timedb.Now()
}

// SetReminder sets a reminder.  NB that it's the caller's job to make sure that
// `content` is somethign sensible.
func (btdb *BibleTagDb) SetReminder(chatid int64, dayspecifier string, dayoffset string, content string) error {
	if _, err := timeoutCalc(btdb.now(),
		dayspecifier, dayoffset, timedb.Location{Location: time.UTC}); err != nil {
		return fmt.Errorf("Error pasing (%s %s): %w", dayspecifier, dayoffset, err)
	}

	err := txutil.TxLoopDb(btdb.db, func(eq sqlx.Ext) error {
		// Get the timezone of the chatid
		var loc timedb.Location
		if err := sqlx.Get(eq,
			&loc,
			`select timezone from chats where chatid=?`,
			chatid); err != nil {
			return fmt.Errorf("Getting timezone for chatid %d: %w", chatid, err)
		}

		dbTime := btdb.now().In(&loc)

		// Calculate the timeout
		timeout, err := timeoutCalc(dbTime, dayspecifier, dayoffset, loc)
		if err != nil {
			return fmt.Errorf("INTERNAL ERROR: Calcuating timeout: %w", err)
		}

		// Insert the reminder into the list
		if _, err := eq.Exec(
			`insert into reminders(chatid, dayspecifier, dayoffset, content, timeout)
			values (?, ?, ?, ?, ?)`,
			chatid, dayspecifier, dayoffset, content, timeout); err != nil {
			return fmt.Errorf("Inserting reminder: %w", err)
		}

		return nil
	})

	if err != nil {
		return err
	}

	btdb.sendRescan()

	return nil
}

type ListReminderResult struct {
	DaySpecifier string
	DayOffset    string
	Content      string
}

func (btdb *BibleTagDb) ListReminders(chatid int64) ([]ListReminderResult, error) {
	var result []ListReminderResult

	if err := btdb.db.Select(&result,
		`select dayspecifier, dayoffset, content from reminders where chatid=?`,
		chatid); err != nil {
		return nil, fmt.Errorf("Listing reminders for chatid %d: %w", chatid, err)
	}

	return result, nil
}

func (btdb *BibleTagDb) DeleteReminder(chatid int64, dayspecifier string, dayoffset string) error {
	if result, err := btdb.db.Exec(
		`delete from reminders where chatid=? and dayspecifier=? and dayoffset=?`,
		chatid, dayspecifier, dayoffset); err != nil {
		return fmt.Errorf("Deleting reminder (%d %s %s): %w", chatid, dayspecifier, dayoffset, err)
	} else if affected, err := result.RowsAffected(); err != nil {
		return fmt.Errorf("Database error getting rows affected: %w", err)
	} else if affected == 0 {
		return fmt.Errorf("No such reminder (%d %s %s)", chatid, dayspecifier, dayoffset)
	}

	btdb.sendRescan()

	return nil
}

func (btdb *BibleTagDb) SetTimezone(chatid int64, TZ *time.Location) error {
	timezone := timedb.Location{Location: TZ}

	err := txutil.TxLoopDb(btdb.db, func(eq sqlx.Ext) error {
		if res, err := eq.Exec(
			`update chats set timezone=? where chatid=?`,
			timezone, chatid); err != nil {
			return fmt.Errorf("Updating timezone for chatid %d: %w", chatid, err)
		} else if affected, err := res.RowsAffected(); err != nil {
			return fmt.Errorf("Database error getting rows affected: %w", err)
		} else if affected == 0 {
			return fmt.Errorf("Couldn't find chatid %d", chatid)
		}

		var reminders []reminderWithTimezone

		// Get a list of all reminders from this chatid
		if err := sqlx.Select(eq, &reminders,
			`select reminderid, chatid, dayspecifier, dayoffset, timezone, content
	  from reminders natural join chats
	  where chatid=?`,
			chatid); err != nil {
			return fmt.Errorf("Getting expired reminders: %w", err)
		}

		// Calculate a new timeout for each of them
		if err := resetTimeouts(eq, btdb.now(), reminders); err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return err
	}

	btdb.sendRescan()

	return nil
}

type dayBitfield uint

var dayspecMap = []struct {
	specifier string
	bits      uint
}{
	{"daily", 0x7f},
	{"sunday", 1 << time.Sunday},
	{"monday", 1 << time.Monday},
	{"tuesday", 1 << time.Tuesday},
	{"wednesday", 1 << time.Wednesday},
	{"thursday", 1 << time.Thursday},
	{"friday", 1 << time.Friday},
	{"saturday", 1 << time.Saturday},
	{"weekdays", 0x3e},
	{"weekends", 0x41},
}

func dayspecParse(dayspec string) (dayBitfield, error) {
	var dbf uint
	specifiers := strings.Split(dayspec, ",")
specloop:
	for _, specifier := range specifiers {
		for _, dsm := range dayspecMap {
			if strings.EqualFold(specifier, dsm.specifier) {
				dbf |= dsm.bits
				continue specloop
			}
		}

		// The specifier wasn't found
		return 0, fmt.Errorf("Unknown specifier %s", specifier)
	}

	if dbf == 0 {
		return 0, fmt.Errorf("Empty dayspec")
	}

	return dayBitfield(dbf), nil
}

func dayspecMatch(days dayBitfield, t timedb.Time) bool {
	day := t.Weekday()

	return (days & (1 << day)) != 0
}

func timeoutCalc(dbnow timedb.Time, dayspec string, dayoffset string, loc timedb.Location) (timedb.Time, error) {
	days, err := dayspecParse(dayspec)
	if err != nil {
		return timedb.Time{}, err
	}

	// FIXME: Accept either "15:04" or "3:04PM"
	doTime, err := time.Parse("15:04", dayoffset)
	if err != nil {
		return timedb.Time{}, fmt.Errorf("Error parsing time of day: %w", err)
	}

	timeout := timedb.Date(dbnow.Year(), dbnow.Month(), dbnow.Day(),
		doTime.Hour(), doTime.Minute(), 0, 0, loc.Location).
		In(&timedb.Location{Location: time.UTC})

	for !timeout.After(dbnow.Time) || !dayspecMatch(days, timeout) {
		timeout = timeout.AddDate(0, 0, 1)
	}

	return timeout, nil
}

type reminderWithTimezone struct {
	ReminderId   int
	ChatId       int64
	DaySpecifier string
	DayOffset    string
	Content      string
	TimeZone     timedb.Location
}

func resetTimeouts(eq sqlx.Ext, dbnow timedb.Time, res []reminderWithTimezone) error {
	for _, r := range res {
		newTimeout, err := timeoutCalc(dbnow, r.DaySpecifier, r.DayOffset, r.TimeZone)
		if err != nil {
			return fmt.Errorf("INTERNAL ERROR calculating new time: %w", err)
		}
		if _, err := eq.Exec(
			`update reminders set timeout=? where reminderid=?`,
			newTimeout, r.ReminderId); err != nil {
			return fmt.Errorf("Updating timeout %v: ", err)
		}
	}

	return nil
}

func (btdb *BibleTagDb) RemiderScan() ([]Reminder, time.Duration, error) {
	if btdb.testTimerMachinery {
		return []Reminder{
			{125, "John 1:1"},
			{123, "John 1:1"},
			{126, "John 1:1"},
			{128, "John 1:1"},
		}, time.Millisecond * 200, nil
	}

	var reminders []Reminder

	var res []reminderWithTimezone

	var dbnow timedb.Time

	var timeoutList []timedb.Time

	err := txutil.TxLoopDb(btdb.db, func(eq sqlx.Ext) error {

		dbnow = btdb.now()

		// Get a list of all expired reminders
		if err := sqlx.Select(eq, &res,
			`select reminderid, chatid, dayspecifier, dayoffset, timezone, content
	  from reminders natural join chats
	  where datetime(timeout) <= datetime(?)
	  order by datetime(timeout)`,
			dbnow); err != nil {
			return fmt.Errorf("Getting expired reminders: %w", err)
		}

		// Calculate a new timeout for each of them
		if err := resetTimeouts(eq, dbnow, res); err != nil {
			return err
		}

		// Find the soonest timeout
		if err := sqlx.Select(eq, &timeoutList,
			`select timeout from reminders order by datetime(timeout) limit 1`); err != nil {
			return fmt.Errorf("Getting next timeout: %w", err)
		}

		return nil
	})

	if err != nil {
		return nil, btdb.defaultTimeout, err
	}

	reminders = make([]Reminder, len(res))
	for i := range res {
		reminders[i].ChatId = res[i].ChatId
		reminders[i].Content = res[i].Content
	}

	nextTimeout := btdb.defaultTimeout

	if len(timeoutList) > 0 {
		nextTimeout = timeoutList[0].Sub(dbnow.Time)
	}

	return reminders, nextTimeout, nil
}

type Reminder struct {
	ChatId  int64
	Content string // Either the passage or the tag
}

// OpenReminderChannel will start up the reminder machinery and return a channel
// to which will be sent reminders at the appropriate times.  It is not
// thread-safe with other reminder operations; the caller is responsible to make
// sure it does not race with open/close channel operations, or with reminder
// creation / deletion / chatid timezone changes.
func (btdb *BibleTagDb) OpenReminderChannel() (chan Reminder, error) {
	if btdb.reminderRecv != nil {
		return nil, fmt.Errorf("Channel already open!")
	}

	// If we're just testing hte timer machinery,
	chansize := 1024
	if btdb.testTimerMachinery {
		chansize = 2
	}
	btdb.reminderRecv = make(chan Reminder, chansize)

	btdb.reminderCommand = make(chan int)

	go btdb.reminderThread()

	return btdb.reminderRecv, nil
}

const (
	reminderCmdRescan = 1
)

// CloseReminderChannel will shut down the reminder machinery and close the
// channel.  It is not thread-safe with other reminder operations; the caller is
// responsible to make sure it does not race with open/close channel operations,
// or with creation / deletion / chatid timezone changes.
func (btdb *BibleTagDb) CloseReminderChannel() error {
	if btdb.reminderCommand == nil {
		return fmt.Errorf("Reminder channel not open")
	}

	close(btdb.reminderCommand)

	return nil
}

func (btdb *BibleTagDb) sendRescan() {
	if btdb.reminderCommand != nil {
		btdb.reminderCommand <- reminderCmdRescan
	}
}

// Sometimes when we want to re-set the timer, it will have fired already (in
// which case timer.Stop() will return false but reading the channel wil block).
// This will put the timer into a known state.
func drainTimer(timer *time.Timer) {
	timer.Stop()
	select {
	case <-timer.C:
		break
	default:
		break
	}
}

func (btdb *BibleTagDb) reminderThread() {
	// We can't create an "empty" timer, so create one that expires immediately;
	// it will be reset after the scan
	timer := time.NewTimer(0)

scanloop:
	for {
		drainTimer(timer)

		reminders, timeout, err := btdb.RemiderScan()
		if err != nil {
			log.Printf("bibletags reminderThread: ReminderScan returned error: %v", err)
		}

		log.Printf("reminderThread: %d reminders, timeout %v", len(reminders), timeout)

		timer.Reset(timeout)

	remindersend:
		for i, reminder := range reminders {
			select {
			case btdb.reminderRecv <- reminder:
				continue remindersend
			default:
				log.Printf("Reminder: Send on reminder Recv would block, dropping %d reminders",
					len(reminders)-i)
				break remindersend
			}
		}

		select {
		case <-timer.C:
			continue scanloop
		case cmd, open := <-btdb.reminderCommand:
			if !open {
				log.Printf("reminderThread: reminderCommand channel closed, exiting thread")
				break scanloop
			}
			switch cmd {
			case reminderCmdRescan:
				log.Printf("reminderThread: Received kick on channel, restarting scan")
				continue scanloop
			}
		}
	}

	drainTimer(timer)
	close(btdb.reminderRecv)
	//close(btdb.reminderCommand)
}

create table chats (
    chatid   integer primary key,
    userid   integer not null,
    timezone text not null,
    bibleid  text,
    unique(chatid, userid)
) strict;

create table tags (
    tagname text collate nocase,
    chatid  integer not null references chats,
    primary key(tagname, chatid)
) strict;

create table passagetags (
    tagname  text collate nocase not null,
    chatid   integer not null references chats,
    passage  text collate nocase not null,
    primary key(tagname, chatid, passage),
    foreign key(tagname, chatid) references tags
) strict;

/* 
 * NB viewchatid, tagchatid, and tagname may be null if
 * the tag (tagchatid, tagname) was deleted, or if the chat
 * viewchatid was deleted.
 */
create table viewlog (
    viewchatid integer references chats, /* The chatid where this was viewed */
    tagchatid  integer, /* The chatid of the tag (either global or == viewchatid). */
    tagname    text collate nocase,
    passage    text collate nocase not null,
    ts         text not null,
    foreign key (tagchatid, tagname) references tags(chatid, tagname)
) strict;

create table reminders (
    reminderid   integer primary key,
    chatid       integer not null,
    dayspecifier text not null,
    dayoffset    text not null,
    content      text not null, /* Either a passage or a tag */
    timeout      text not null,
    unique(chatid, dayspecifier, dayoffset)
) strict;

/*
 * Insert a global chat with id 0.  NB this value must be kept in sync with
 * chats.go:CHATID_GLOBAL
 */
insert into chats(chatid, userid, timezone) values(0, 0, "UTC");
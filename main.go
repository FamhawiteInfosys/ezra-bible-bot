package main

/* Ezra Bible Bot
Copyright (C) 2022  George Dunlap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"gitlab.com/martyros/ezra-bible-bot/ezrabot"
)

const (
	BOT_KEY_ENV_VAR = "EZRA_BOT_TOKEN"
	API_KEY_ENV_VAR = "API_BIBLE_API_KEY"
	TAGSDB_ENV_VAR  = "TAGS_DB_FILE"
)

func getEnvOrPanic(envvar string) string {
	if key, prs := os.LookupEnv(envvar); prs {
		return key
	} else {
		log.Fatalf("%s not defined", envvar)
	}
	return ""
}

func main() {
	botkey := getEnvOrPanic(BOT_KEY_ENV_VAR)
	apikey := getEnvOrPanic(API_KEY_ENV_VAR)
	dbFile := getEnvOrPanic(TAGSDB_ENV_VAR)

	bot, err := tgbotapi.NewBotAPI(botkey)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)

	eb, err := ezrabot.Open(apikey, dbFile)
	if err != nil {
		log.Fatalf("Opening ezrabot: %v", err)
	}

	remindersDone := make(chan struct{})
	rchan, err := eb.OpenReminderChannel()
	if err != nil {
		log.Fatalf("Opening reminder channel: %v", err)
	}

	go func() {
		log.Printf("main reminderthread: listening on rchan")
		for reminder := range rchan {
			log.Printf("main reminderthread: received reminder %v", reminder)
			msg := tgbotapi.NewMessage(reminder.ChatId, reminder.Response)
			msg.ParseMode = "HTML"
			if _, err := bot.Send(msg); err != nil {
				log.Printf("Error sending message: %v", err)
			}
		}

		log.Printf("main reminderthread: rchan closed, closing remindersDone")
		close(remindersDone)
	}()

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 45

	updates := bot.GetUpdatesChan(u)

	// Shut down gracefully on interrupt
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

		sig := <-c

		log.Printf("Received signal %v, stopping updates", sig)

		eb.CloseReminderChannel()

		bot.StopReceivingUpdates()
	}()

	log.Printf("main: listeng in updates")
	for update := range updates {
		switch {
		case update.Message != nil:
			log.Printf("[%d] %s", update.Message.Chat.ID, update.Message.Text)

			response := eb.HandleCommand(ezrabot.Command{
				Command: update.Message.Text,
				ChatId:  update.Message.Chat.ID,
			})

			if response.Response != "" {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, response.Response)
				msg.ReplyToMessageID = update.Message.MessageID
				msg.ParseMode = "HTML"
				if _, err := bot.Send(msg); err != nil {
					log.Printf("Error sending message: %v", err)
				}
			}
		default:
			log.Printf("Unhandled message type: %v", update)
		}
	}

	log.Printf("Main: Waiting for remindersDone to close")

	<-remindersDone
}

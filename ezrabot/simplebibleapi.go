package ezrabot

/* Ezra Bible Bot
Copyright (C) 2022  George Dunlap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
)

// Constants to be used in testing
const (
	fakeApiKey = "FAKE"
)

var badPassages = map[string]struct{}{
	"Flubarg 1:1":        {},
	"3 Coranthians 5:17": {},
	"Golotians 2:20":     {},
	"Lomans 12:1":        {},
	"Yohn 14:21":         {},
}

type Response struct {
	Response string
	IsError  bool
}

// TODO: api.bible has some typo checking; so that "Johnx 1:1" for instance
// succeeds.  We'd ideally like to store the actual passage, not the typo'd
// passage; there's probably a way to do that with the API, but something
// further down the line.
func (eb *EzraBot) checkPassage(query string) (bool, error) {
	if eb.isFake {
		_, ok := badPassages[query]
		return !ok, nil
	}

	if rsp, _, err := eb.bible.SearchApi.SearchBible(eb.ctx, eb.bibleId).Query(query).Execute(); err != nil {
		log.Printf("checkPassage: SearchBible %s resulted in an error: %v", query, err)
		return false, fmt.Errorf("Executing SearchBible query: %w", err)

	} else if len(rsp.Data.Passages) < 1 {
		log.Printf("checkPassage: SearchBible %s resulted in no passages", query)
		return false, nil
	}
	return true, nil
}

func (eb *EzraBot) queryPassage(chatid int64, query string) Response {
	if eb.isFake {
		if _, ok := badPassages[query]; !ok {
			return Response{"A wicked and adulterous generation looks for a sign," +
				" but none will be given it except the sign of Jonah." +
				"  Jesus then left them and went away.", false}
		} else {
			return Response{"Sorry, your query returned no results", true}
		}
	}

	if rsp, _, err := eb.bible.SearchApi.SearchBible(eb.ctx, eb.bibleId).Query(query).Execute(); err != nil {
		log.Printf("queryPassage: SearchBible %s resulted in an error: %v", query, err)
		return Response{"Sorry, there was an internal error", true}

	} else if len(rsp.Data.Passages) < 1 {
		log.Printf("queryPassage: SearchBible %s resulted in no passages", query)
		return Response{"Sorry, your query returned no results", true}
	} else {
		passageId := rsp.Data.Passages[0].Id
		log.Printf("queryPassage: SearchBible %s returned PassageId %s", query, passageId)

		if rsp, _, err := eb.bible.PassagesApi.GetPassage(eb.ctx, eb.bibleId, passageId).ContentType("text").IncludeVerseNumbers(false).Execute(); err != nil {
			log.Printf("queryPassage: GetPassage %s resulted in an error %v", passageId, err)
			return Response{"Sorry, your query resulted in an error", true}
		} else {
			// Paragraph tabs look strange; just get rid of all unnecessary
			// whitespace. TODO: See if there's a major performance difference
			// between this and a regexp solition; see
			// https://stackoverflow.com/questions/37290693/how-to-remove-redundant-spaces-whitespace-from-a-string-in-golang

			go eb.reportFUMS(chatid, rsp.Meta.FumsId)

			return Response{strings.Join(strings.Fields(strings.TrimSpace(rsp.Data.Content)), " "), false}
		}
	}
}

func (eb *EzraBot) reportFUMS(chatid int64, fumsID string) {
	// The only info we have is the chatid; it doesn't make much sense in our
	// context to distinguish "device id" from "session id".  So hash chatid
	// once and use it for both.
	h := sha256.New()
	binary.Write(h, binary.LittleEndian, chatid)
	chatidHash := fmt.Sprintf("%x", h.Sum(nil))

	fumsUrl := fmt.Sprintf("https://fums.api.bible/f3?t=%s&dId=%s&sId=%s",
		fumsID, chatidHash, chatidHash)

	res, err := http.Get(fumsUrl)
	if err != nil {
		log.Printf("Error submitting FUMS url %s: %v", fumsUrl, err)
		return
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Printf("reportFUMS: ERROR reading body of response: %v", err)
	}
	res.Body.Close()

	if res.StatusCode > 299 {
		log.Printf("FUMS report returned ERROR status code %d and\nbody: %s", res.StatusCode, body)
		return
	}
	log.Printf("FUMS report successful (code %d)", res.StatusCode)
}

package ezrabot

import (
	"io/ioutil"
	"os"
	"testing"
)

type query struct {
	query   string
	isError bool
}

const (
	API_KEY_ENV_VAR = "API_BIBLE_API_KEY"
)

func TestCmd(t *testing.T) {
	// Use a real API key if it exists
	apikey := fakeApiKey
	if key, prs := os.LookupEnv(API_KEY_ENV_VAR); prs {
		t.Logf("%s found, using real API key", API_KEY_ENV_VAR)
		apikey = key
	} else {
		t.Logf("%s not found, using fake API", API_KEY_ENV_VAR)
	}

	dbFile, err := ioutil.TempFile("", "bibledb*.sqlite")
	if err != nil {
		t.Errorf("Opening temp file for db operations: %v", err)
		return
	}

	t.Logf("Creating DB in file %s", dbFile.Name())

	eb, err := Open(apikey, dbFile.Name())
	if err != nil {
		t.Errorf("ERROR: Opening ezrabot: %v", err)
		return
	}

	chats := []struct {
		chatid  int64
		queries []query
	}{
		{12098, []query{
			{"/start", false},
			{"John 1:1", false},
			{"  John 1:1", false},
			{"John 1:1  ", false},
			{"Flubarg 1:1", true},
			{"/listtags", false},
			{"/listtags  ", false},
			{"/showtag", true},
			{"NewLif", true},
			{"NewLife", false},
			{"newlife", false},
			{":NewLife", true},
			{"  NewLife", false},
			{"NewLife  ", false},
			{"/showtag NewLife", false},
			{"/showtag newlife", false},
			{"/showtag :NewLife", true},
			{"/showtag   NewLife", false},
			{"/showtag NewLife  ", false},
			{"/get John 1:1", false},
			{"/get  John 1:1", false},
			{"/get John 1:1  ", false},
			{"/get Flubarg 1:1", true},
			{"/get NewLif", true},
			{"/get NewLife", false},
			{"/get newlife", false},
			{"/get :NewLife", true},
			{"/get  NewLife", false},
			{"/get NewLife  ", false},
			{"/showtag@EzraBibleBot NewLife  ", false},
			{"/get@EzraBibleBot John 1:1", false},
			{"/get@EzraBibleBot  John 1:1", false},
			{"/listtags@EzraBibleBot", false},
			{"/listtags@EzraBibleBot  ", false},
			{"/listreminders", false},
			{"/reminder daily 7:00 John 1:1", false},
			{"/reminder daily 7:00 NewLife", true},
			{"/reminder daily 7:01 NewLife", false},
			{"/reminder daily 10:30 Chrislikeness", true},
			{"/reminder daily 10:30 Christlikeness", false},
			{"/reminder friday 10:30 Prayer", true},
			{"/reminder monday,friday 10:30 Prayer", true},
			{"/listreminders", false},
			{"/deletereminder daily 7:01", false},
			{"/deletereminder daily 7:10", true},
			{"/settimezone Europe/London", false},
			{"/settimezone Australio/Sydney", true},
			{"/settimezone Australia/Sydney", false},

			{"/tag NewLife John 10:10", true},
			{"/newtag NewLife", false},
			{"/tag NewLife John 10:10", false},
			{"/showtag NewLife", false},
			{"/newtag Prayer", false},
			{"/newtag Prayer", true},
			{"/showtag Prayer", false},
			{"/tag Player 1 John 5:14", true},
			{"/tag Prayer 1 John 5:14", false},
			{"/tag Prayer Ephesians 1:18", false},
			{"/tag Prayer NewLife", true},
			{"/tag Prayer 1 2 Corinthian 5: 17", true},
			{"/showtag Prayer", false},
			{"/tag Prayer Isaiah 53:6", false},
			{"/untag Prayer Isaiah 53:6", false},
			{"/untag Prayer Isaiah 53:6", true},
			{"/deletetag Prayer", false},
			{"/deletetag Prayer", true},
		}},
	}

	for _, chat := range chats {
		for _, query := range chat.queries {
			t.Logf("%d: %s", chat.chatid, query.query)
			r := eb.HandleCommand(Command{ChatId: chat.chatid, Command: query.query})
			t.Logf("ebb: %s", r.Response)
			if r.IsError != query.isError {
				t.Errorf("ERROR: Query %s expected iserror %v, got %v",
					query.query, query.isError, r.IsError)
			}
		}
	}
}

package ezrabot

/* Ezra Bible Bot
Copyright (C) 2022  George Dunlap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"fmt"
	"log"

	bibletags "gitlab.com/martyros/ezra-bible-bot/bible-tags"
)

var initialTags = map[string][]string{
	"NewLife": {
		"2 Corinthians 5:17",
		"Galatians 2:20",
		"Romans 12:1",
		"John 14:21",
		"2 Timothy 3:16",
		"Joshua 1:8",
		"John 15:7",
		"Philippians 4:6-7",
		"Matthew 18:20",
		"Hebrews 10:24-25",
		"Matthew 4:19",
		"Romans 1:16"},
	"ProclaimChrist": {
		"Romans 3:23",
		"Isaiah 53:6",
		"Romans 6:23",
		"Hebrews 9:27",
		"Romans 5:8",
		"1 Peter 3:18",
		"Ephesians 2:8-9",
		"Titus 3:5",
		"John 1:12",
		"Revelation 3:20",
		"1 John 5:13",
		"John 5:24"},
	"RelyOnGod": {
		"1 Corinthians 3:16",
		"1 Corinthians 2:12",
		"Isaiah 41:10",
		"Philippians 4:13",
		"Lamentations 3:22-23",
		"Numbers 23:19",
		"Isaiah 26:3",
		"1 Peter 5:7",
		"Romans 8:23",
		"Philippians 4:19",
		"Hebrews 9:27"},
	"ChristsDisciple": {
		"Matthew 6:33",
		"Luke 9:23",
		"1 John 2:15-16",
		"Romans 12:2",
		"1 Corinthians 15:58",
		"Hebrews 12:3",
		"Mark 10:45",
		"2 Corinthians 4:5",
		"Proverbs 3:9-10",
		"2 Corinthians 9:6-7",
		"Acts 1:8",
		"Matthew 28:19-20"},
	"Christlikeness": {
		"John 13:34-35",
		"1 John 3:18",
		"Philippians 2:3-4",
		"1 Peter 5:5-6",
		"Ephesians 5:3",
		"1 Peter 2:11",
		"Leviticus 19:11",
		"Acts 24:16",
		"Hebrews 11:6",
		"Romans 4:20-21",
		"Galatians 6:9-10",
		"Matthew 5:16"},
}

func (eb *EzraBot) populateTags() error {
	if tags, err := eb.btdb.ListTags(bibletags.CHATID_GLOBAL); err != nil {
		return fmt.Errorf("Getting global tags: %w", err)
	} else if len(tags) > 0 {
		log.Printf("Already have %d global tags, not pre-populating", len(tags))
		return nil
	}

	for tagname, passages := range initialTags {
		log.Printf("Pre-populating tag %s", tagname)
		if err := eb.btdb.NewTag(bibletags.CHATID_GLOBAL, tagname); err != nil {
			return fmt.Errorf("Creating initial tag %s: %w", tagname, err)
		}

		for _, passage := range passages {
			log.Printf("Tagging passage %s", passage)
			if ok, err := eb.checkPassage(passage); err != nil {
				return err
			} else if !ok {
				return fmt.Errorf("Invalid initial passage %s", passage)
			}
			if err := eb.btdb.TagPassage(bibletags.CHATID_GLOBAL, tagname, passage); err != nil {
				return fmt.Errorf("Tagging passage %s", passage)
			}
		}
	}

	return nil
}

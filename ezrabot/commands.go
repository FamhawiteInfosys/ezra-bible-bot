// Package ezrabot implements the command-response part of the ezra-bible-bot.
package ezrabot

/* Ezra Bible Bot
Copyright (C) 2022  George Dunlap

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import (
	"context"
	"fmt"
	"log"
	"strings"
	"text/template"
	"time"

	bibleapi "gitlab.com/martyros/go-api-bible"

	bibletags "gitlab.com/martyros/ezra-bible-bot/bible-tags"
)

const (
	BIBLEAPI_ABBREVIATION_WEB = "WEB"
)

type Command struct {
	Command string
	ChatId  int64
}

type EzraBot struct {
	btdb  *bibletags.BibleTagDb
	bible *bibleapi.APIClient
	// FIXME Not sure what we should do here when we parallelize; make a new context for every request?
	ctx context.Context

	rchanIsOpen bool

	isFake bool

	// Things to be diversified later
	bibleId string
}

type Reminder struct {
	ChatId   int64
	Response string
}

func Open(apikey string, tagsdbfile string) (*EzraBot, error) {
	eb := &EzraBot{}

	if apikey == "FAKE" {
		log.Printf("Fake API key, using faked api.bible backend")
		eb.isFake = true
	} else {
		eb.bible = bibleapi.NewAPIClient(bibleapi.NewConfiguration())

		eb.ctx = context.WithValue(context.Background(),
			bibleapi.ContextAPIKeys,
			map[string]bibleapi.APIKey{
				"ApiKeyAuth": {
					Key: apikey,
				},
			})

		// Get the Bible ID
		if rsp, _, err := eb.bible.BiblesApi.GetBibles(eb.ctx).Abbreviation(BIBLEAPI_ABBREVIATION_WEB).Execute(); err != nil {
			return nil, fmt.Errorf("Error getting Bible ID for %v: %w\n", BIBLEAPI_ABBREVIATION_WEB, err)
		} else if len(rsp.Data) < 1 {
			return nil, fmt.Errorf("Getting Bible ID: No data in response!")
		} else {
			eb.bibleId = rsp.Data[0].Id
		}
		log.Printf("Bible ID for %s: %v", BIBLEAPI_ABBREVIATION_WEB, eb.bibleId)
	}

	if btdb, err := bibletags.OpenDb(tagsdbfile); err != nil {
		return nil, fmt.Errorf("Opening bibletags file %s: %w", tagsdbfile, err)
	} else {
		eb.btdb = btdb
	}

	if err := eb.populateTags(); err != nil {
		return nil, fmt.Errorf("Populating tags: %w", err)
	}

	return eb, nil
}

func (eb *EzraBot) OpenReminderChannel() (chan Reminder, error) {
	if eb.rchanIsOpen {
		return nil, fmt.Errorf("Channel already open!")
	}

	brchan, err := eb.btdb.OpenReminderChannel()
	if err != nil {
		return nil, fmt.Errorf("Opening bibletags reminder channel: %w", err)
	}

	rchan := make(chan Reminder, 1024)

	go eb.reminderThread(brchan, rchan)

	return rchan, nil
}

func (eb *EzraBot) reminderThread(brchan chan bibletags.Reminder, rchan chan Reminder) {
	log.Printf("eb reminderthread: Listening on brchan")
	for reminder := range brchan {
		passage := reminder.Content
		if !strings.Contains(reminder.Content, " ") {
			if res, err := eb.btdb.GetTag(reminder.ChatId, reminder.Content); err != nil {
				log.Printf("ERROR Getting reminder tag %s: %v", reminder.Content, err)
				continue
			} else {
				passage = res
			}
		}
		ptext := eb.queryPassage(reminder.ChatId, passage)
		if ptext.IsError {
			log.Printf("ERROR Getting passage %s: %v", passage, ptext)
			continue
		}

		response := fmt.Sprintf("%s <i>%s</i>", ptext.Response, passage)
		if passage != reminder.Content {
			// This was a tag, so add the tag in
			response += " (" + reminder.Content + ")"
		}

		rchan <- Reminder{ChatId: reminder.ChatId,
			Response: response}
	}

	log.Printf("eb reminderthread: brchan closed, closing rchan")
	close(rchan)
}

func (eb *EzraBot) CloseReminderChannel() error {
	// This should close brchan, which should in turn close rchan
	err := eb.btdb.CloseReminderChannel()
	eb.rchanIsOpen = false
	return err
}

var cmdTable = map[string]func(*EzraBot, int64, []string) Response{}

func init() {
	cmdTable["/start"] = CmdStart
}

func CmdStart(eb *EzraBot, chatid int64, args []string) Response {
	return Response{"Welcome to EzraBot!", false}
}

func init() {
	cmdTable["/listtags"] = CmdListTags
}

var tmplListTags = template.Must(template.New("ListTags").Parse(
	`Tags (bolded tags are global):
{{range . -}}
{{if .IsGlobal}}<b>{{.Tagname}}</b>{{else}}{{.Tagname}}{{end}}
{{end -}}`))

func CmdListTags(eb *EzraBot, chatid int64, args []string) Response {
	tags, err := eb.btdb.ListTags(chatid)
	if err != nil {
		log.Printf("Error listing tags: %v", err)
		return Response{fmt.Sprintf("Error listing tags: %v", err), true}
	}
	var rtext strings.Builder
	if err := tmplListTags.Execute(&rtext, tags); err != nil {
		log.Printf("Error executing template: %v", err)
		return Response{"Sorry, there was an internal error", true}
	}
	return Response{rtext.String(), false}
}

func init() {
	cmdTable["/showtag"] = CmdShowTag
}

var tmplShowTag = template.Must(template.New("ShowTags").Parse(
	`Passages tagged with <b>{{.Tagname}}</b> (bolded passages are global):
{{range .Passages -}}
{{if .IsGlobal}}<b>{{.Passage}}</b>{{else}}{{.Passage}}{{end}}
{{end -}}`))

func CmdShowTag(eb *EzraBot, chatid int64, args []string) Response {
	if len(args) != 1 {
		return Response{"Usage:\n/showtags <i>tagname</i>", true}
	}
	tagname := args[0]
	passages, err := eb.btdb.ShowTagPassages(chatid, tagname)
	if err != nil {
		log.Printf("Error getting passages for tag (%d %s): %v", chatid, tagname, err)
		return Response{fmt.Sprintf("Error showing tag: %v", err), true}
	}

	var rtext strings.Builder
	if err := tmplShowTag.Execute(&rtext, map[string]interface{}{"Tagname": tagname, "Passages": passages}); err != nil {
		log.Printf("Error executing template: %v", err)
		return Response{"Sorry, there was an internal error", true}
	}
	return Response{rtext.String(), false}
}

func init() {
	cmdTable["/get"] = CmdGet
}

func CmdGet(eb *EzraBot, chatid int64, args []string) Response {
	if len(args) == 1 {
		if passage, err := eb.btdb.GetTag(chatid, args[0]); err != nil {
			log.Printf("ERROR getting tag (%d, %s): %v", chatid, args[0], err)
			return Response{"Couldn't get tag", true}
		} else {
			ptext := eb.queryPassage(chatid, passage)
			if ptext.IsError {
				return ptext
			}
			return Response{fmt.Sprintf("%s <i>%s</i>", ptext.Response, passage), false}
		}
	}
	return eb.queryPassage(chatid, strings.Join(args, " "))

}

func init() {
	cmdTable["/reminder"] = CmdReminder
}

func CmdReminder(eb *EzraBot, chatid int64, args []string) Response {
	if len(args) < 3 {
		return Response{"Usage:\n/reminder daily <i>time</i> <i>content</i>", true}
	}

	content := strings.Join(args[2:], " ")
	if len(args) > 3 {
		if prs, err := eb.checkPassage(content); err != nil {
			return Response{fmt.Sprintf("Query error: %v", err), true}
		} else if !prs {
			return Response{fmt.Sprintf("Couldn't find passage %s", content), true}
		}
	} else {
		if _, err := eb.btdb.ShowTagPassages(chatid, content); err != nil {
			return Response{fmt.Sprintf("Error verifying tag %s: %v", content, err), true}
		}
	}

	if err := eb.btdb.SetReminder(chatid, args[0], args[1], content); err != nil {
		return Response{fmt.Sprintf("Error setting reminder: %v", err), true}
	}

	return Response{fmt.Sprintf("Reminder (%s %s %s) set", args[0], args[1], content), false}
}

func init() {
	cmdTable["/listreminders"] = CmdListReminders
}

var tmplListReminders = template.Must(template.New("ListReminders").Parse(
	`Reminders:
{{range . -}}
{{.DaySpecifier}} {{.DayOffset}} {{.Content}}
{{end -}}`))

func CmdListReminders(eb *EzraBot, chatid int64, args []string) Response {
	reminders, err := eb.btdb.ListReminders(chatid)
	if err != nil {
		log.Printf("Error listing reminders: %v", err)
		return Response{fmt.Sprintf("Error listing reminders: %v", err), true}
	}

	var rtext strings.Builder
	if err := tmplListReminders.Execute(&rtext, reminders); err != nil {
		log.Printf("Error executing template: %v", err)
		return Response{"Sorry, there was an internal error", true}
	}
	return Response{rtext.String(), false}
}

func init() {
	cmdTable["/deletereminder"] = CmdDeleteReminder
}

func CmdDeleteReminder(eb *EzraBot, chatid int64, args []string) Response {
	if len(args) < 2 {
		return Response{"Usage:\n/deletereminder daily <i>time</i>", true}
	}

	if err := eb.btdb.DeleteReminder(chatid, args[0], args[1]); err != nil {
		return Response{fmt.Sprintf("Deleting reminder: %v", err), true}
	}

	return Response{fmt.Sprintf("Deleted reminder at %s %s", args[0], args[1]), false}
}

func init() {
	cmdTable["/settimezone"] = CmdSetTimezone
}

func CmdSetTimezone(eb *EzraBot, chatid int64, args []string) Response {
	if len(args) != 1 {
		return Response{"Usage:\n/settimezone <i>timezone</i>", true}
	}

	if tz, err := time.LoadLocation(args[0]); err != nil {
		return Response{fmt.Sprintf("Parsing timezone %s: %v", args[0], err), true}
	} else if err := eb.btdb.SetTimezone(chatid, tz); err != nil {
		return Response{fmt.Sprintf("Setting timezone to %s: %v", args[0], err), true}
	}

	return Response{fmt.Sprintf("Set timezone to %s", args[0]), false}
}

func init() {
	cmdTable["/newtag"] = CmdNewTag
}

func CmdNewTag(eb *EzraBot, chatid int64, args []string) Response {
	if len(args) != 1 {
		return Response{"Usage:\n/newtag <i>tagname</i>", true}
	}

	if err := eb.btdb.NewTag(chatid, args[0]); err != nil {
		return Response{fmt.Sprintf("Adding tag: %v", err), true}
	}

	return Response{fmt.Sprintf("Tag %s created", args[0]), false}
}

func init() {
	cmdTable["/deletetag"] = CmdDeleteTag
}

func CmdDeleteTag(eb *EzraBot, chatid int64, args []string) Response {
	if len(args) != 1 {
		return Response{"Usage:\n/deletetag <i>tagname</i>", true}
	}

	if err := eb.btdb.DeleteTag(chatid, args[0]); err != nil {
		return Response{fmt.Sprintf("Deleting tag: %v", err), true}
	}

	return Response{fmt.Sprintf("Tag %s deleted", args[0]), false}
}

func init() {
	cmdTable["/tag"] = CmdTag
}

func CmdTag(eb *EzraBot, chatid int64, args []string) Response {
	// "Passage" must be 2 or 3 elements
	if len(args) < 3 || len(args) > 4 {
		return Response{"Usage:\n/tag <i>tagname</i> <i>passage</i>", true}
	}

	tagname := args[0]
	passage := strings.Join(args[1:], " ")

	if prs, err := eb.checkPassage(passage); err != nil {
		return Response{fmt.Sprintf("Query error: %v", err), true}
	} else if !prs {
		return Response{fmt.Sprintf("Couldn't find passage %s", passage), true}
	}

	if err := eb.btdb.TagPassage(chatid, tagname, passage); err != nil {
		return Response{fmt.Sprintf("Adding tag: %v", err), true}
	}

	return Response{fmt.Sprintf("Added tag %s to passage %s ", tagname, passage), false}
}

func init() {
	cmdTable["/untag"] = CmdUntag
}

func CmdUntag(eb *EzraBot, chatid int64, args []string) Response {
	if len(args) < 3 {
		return Response{"Usage:\n/untag <i>tagname</i> <i>passage</i>", true}
	}

	tagname := args[0]
	passage := strings.Join(args[1:], " ")

	if err := eb.btdb.UntagPassage(chatid, tagname, passage); err != nil {
		return Response{fmt.Sprintf("Removing tag: %v", err), true}
	}

	return Response{fmt.Sprintf("Removed tag %s from paassage %s ", tagname, passage), false}
}

func (eb *EzraBot) HandleCommand(cmd Command) Response {

	if err := eb.btdb.ReifyChat(cmd.ChatId, bibletags.USERID_NONE); err != nil {
		log.Printf("ERROR Reifing chat %d: %v", cmd.ChatId, err)
	}

	fields := strings.Fields(cmd.Command)

	fields[0] = strings.TrimSuffix(fields[0], "@EzraBibleBot")

	// Treat anything without a slash as `/get <query>`
	if !strings.HasPrefix(fields[0], "/") {
		fields = append([]string{"/get"}, fields...)
	}

	if f, prs := cmdTable[fields[0]]; prs {
		return f(eb, cmd.ChatId, fields[1:])
	} else {
		return Response{"Unknown command", true}
	}
}

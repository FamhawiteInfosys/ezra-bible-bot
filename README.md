# Introduction (and roadmap)

This is a gentle introduction to the ideas behind EzraBibleBot.  It's also meant
to be a roadmap; by writing a description of the finished product as
documentation, it forces you to think about a lot of the use cases and issues.
Headings with a :white_check_mark: in the title indicate features which have
been implemented; headings with a :construction: indicate features which have
not yet been implemented.

## Using EzraBibleBot in private conversations

The easiest way to start using EzraBibleBot is to open up a private conversation
with it.

Go to 'Chats', click the 'New chat' icon in the upper right, and then in the
search bar type `@EzraBibleBot`.  This will create a private conversation with
EzraBibleBot.

### Asking for specific passages :white_check_mark:

To begin with, you can open a private conversation with @EzraBibleBot for
specific scripture passages:

```
John 1:1
```

```
In the beginning was the Word, and the Word was with God, and the Word was God.
```

### Tagged passages :white_check_mark:

EzraBibleBot has a number of "tags" which contain a number of passages by topic.
For instance, the `NewLife` tag contains 12 scripture passages from the
Navigator's Topical Memory System, Series A, "Live the New Life".

Rather than asking for a specific passage, you can ask EzraBibleBot to give you
some passage tagged with a given tag.

```
NewLife
```

```
John 15:7 If you remain in me, and my words remain in you, you will ask
whatever you desire, and it will be done for you.
```

The passages aren't completely random.  If a passage has been seen in the last
year, it will be less likely to be chosen; the more recently it's been seen, the
less likely (although it will never be zero).

Capitalizing the first letter of words (known as "CamelCase" in the computer
industry) is encouraged for readability when creating the tags; but tag case is
ignored in commands to make typing easier:

```
newlife
```

```
John 15:7 If you remain in me, and my words remain in you, you will ask
whatever you desire, and it will be done for you.
```

### Discovering tags :white_check_mark:

Tags can be listed with the `/listtags` command.  This lists the global tags,
and any chat-local tags which may have been made.  (See below for chat-local
tags.)

Individual passages tagged with a given tag can be listed using the
`/showtag <tag>` command.

You can also explore global tags at *insert.web.address.here*

### Asking for reminders to be sent at a specific time :white_check_mark:

You can ask for Bible verses to be sent to you at a specific time.  For
instance, the following will send you John 3:16 every morning at 7am:

```
/reminder daily 7:00 John 3:16
```

That might get a bit boring; so instead you can send yourself a reminder of a
specific tag:

```
/reminder daily 7:00 NewLife
```

This will rotate semi-randomly (as described in the tags section) between the 12
passages tagged with NewLife.

Only one reminder can be listed for a given time.

You can list your reminders:

```
/listreminders
```

```
Here are your reminders:
1 - weekdays 7am GodsGlory
2 - weekdays 12pm DependenceOnGod
3 - weekdays 5pm Thankfulness
4 - saturday 9am Celebration
5 - sunday 9am Worship
```

The first argument is the "dayspec".  This can be a single day (case insensitive):

```
/reminder monday 9am GodsGlory
```

Or a list of days:

```
/reminder monday,wednesday,friday 9am GodsGlory
```

It can also be "daily" (every day), "weekdays" (Monday-Friday), or "weekends"
(Saturday/Sunday).  These can also be combined; e.g. the following reminder will be
set for Saturday, Sunday, and Monday:

```
/reminder weekends,monday 9am GodsGlory
```

:construction: Right now, only 24-hour time is implemented (i.e., 7:00 is 7am,
19:00 is 7pm.)

And you can delete reminders, using the dayspec / time.  For instance, given the last response above, the following command:

```
/deletereminder daily 7:00
```

Would delete the `daily 7:00` reminder.  This does mean that you can only have a single reminder for a single dayspec/time; setting a second reminder with the same parameters will return an error.

The default timezone is **Europe/London**.  You can set your timezone as follows:

```
/settimezone Australia/Sydney
```

If you already have reminders, these reminders will be reconfigured according to
the new timezone.  This means when you travel you have only to change your
timezone to get all your reminders in local time.

## Group conversations

You can also invite EzraBibleBot to your group chats.  Functionality is similar,
except that EzraBibleBot cannot read all messages in the group chats; it can
only see messages directed at it.  You can direct messages at it in one two ways:

1. **Use a slash command** :white_check_mark: If EzraBibleBot is in your chat, you can type `/`
and it will bring up a list of commands available to be used.  `/get` is the
command for reading a Bible verse; the same format can be used for either
passages or tagged passages.   I.e., the following should both work:

```
/get@EzraBibleBot John 3:16
```

```
/get@EzraBibleBot NewLife
```

2. **Reply to a message sent from EzraBibleBot**  If you have a message from
EzraBibleBot handy, you can long-press the message to bring up a context menu;
in the menu is the 'Reply' option.  Unless you're replying to a direct question,
EzraBibleBot will ignore whatever it is you're replying to and consider the
message in isolation.

For verse selection purposes, every chat is considered separately.  If you've
just seen a particular verse from a given tag in your own pesonal chat, that
won't affect the probability of seeing that verse when requesteng a tag in a
group chat you're in, or vice versa.

Reminders work similarly; for instance, if you have a Telegram group for a
prayer group that meets regularly at 12pm on Fridays, you can ask EzraBibleBot
to send a verse about praying for each other at 11am:

```
/reminder Friday 11am PrayForMe
```

Don't forget to `/settimezone` in the group chat as well, or you'll be getting
your reminders in UTC.

## Making your own tags :construction:

EzraBibleBot has a number of global tags available to everyone.  But you can
create your own local tags.

To **create a new local tag**:

```
/newtag EvangelismEncouragement
```

You can **tag passages** with the new tag:

```
/tag EvangelismEncouragement Matthew 9:27
```

And of course you can show passages for your own tags:

```
/showtag EvangelismEncouragement
```

You can **un-tag passages**

```
/untag EvangelismEncouragement Matthew 9:27
```

Or

```
/untag EvangelismEncouragement 3
```

Case is also ignored when dealing with passages:

```
/untag evangelismencouragement matthew 9:27
```

And you can **delete an entire tag**

```
/deletetag EvangelismEncouragement
```

## Tags in groups :construction:

Tags are per "chat"; which means you will have one set of tags for your private
chat with EzraBibleBot, but those tags will not be available in groups in which
both you & EzraBibleBot are members.

Any member of the group can add tags to EzraBibleBot.

Additionally, you can import one of your personal tags:

```
/importtag@EzraBibleBot PrayForMe
```

This will create the `PrayForMe` tag if it doesn't exist, and copy all passages
from your personal `PrayForMe` tag into the group `PrayForMe` tag.

## Making suggestions for global tags :construction:

# Data storage and privacy

Data about requests of passages directly is not stored by EzraBibleBot. However,
the API we use to provide Bible verses (api.bible, provided by the American
Bible Society for free) requests that we submit to them usage statistics.  We
anonymize chat IDs and provide this information to them.

In order to implement the functionality to make recently-seen tagged passages
less likely to come up than less-recently-seen passages, as well as to be able
to understand the usage and identify popular tags, EzraBibleBot keeps a record
of each time a passage was viewed by a given tag for a given chat.

# Random ideas to fill out

 - Adding tag descriptions, particularly for global tags, so you can figure out
   what they're meant to be about
 - Making suggestions for global tags
 - "Tag groups": level-2 tags, that switch between tags the same way tags switch
   between passages
 - "Reminder plans": Pre-set reminders throughout the week 
 - "Clone global tag", and let local tags "shadow" global tags, rather than
   having both? Then you could clone & delete

# Clean-ups to do at some point

- When listing reminders, order them by time of day
- Log to both stdout and a file, so we can go back and look
- Make a static ezrabot web page at gitlab.io?  Point to it in the welcome message?
- Set timezone by sending a location.  Warn when TZ is unset?
- Try showing a small selection of the verse when listing passages of a tag
- Empty results to "list" commands should be presented differently
- Improve error messages
- Convert passages into canonical form before storing (e.g., "Johnx 1:1")
- Fork off goroutines for requests / tasks which may block / whatever
- Add "btdb.checkTag" functionality to avoid using ShowTagPassages to check to
  see if a tag exists

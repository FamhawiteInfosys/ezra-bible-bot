# Commands

To set commands, contact the BotFather and send the following messages:

```
/setcommands
```

```
@EzraBibleBot
```

```
get - Get a passage or tag
listtags - Get a list of tags
showtag - Show passages tagged with the given tag
reminder - Set a schedule for the passage or tag to be sent to this chat
listreminders - List current reminders for this chat
deletereminder - Delete a given reminder
settimezone - Set the timezone for reminder delivery (default UTC)
newtag - Create a tag local to this chat
deletetag - Delete a local tag
tag - Tag a passage with a local tag
untag - Untag a passage with a local tag
```